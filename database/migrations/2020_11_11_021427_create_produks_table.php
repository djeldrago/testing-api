<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_produk')->nullable();
            $table->text('deskripsi_produk')->nullable();
            $table->string('gambar_produk')->nullable();
            $table->foreign('kategori_produk_id')->references('id')->on('kategori_produks')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('stok')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produks');
    }
}
