<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{
    

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction_logs';

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'transaction_category', 'transaction_amount'];

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return relation from product model
     *
     */
    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return query of stock_in transaction_category
     *
     * @param $query
     */
    public function scopeStockIn($query)
    {
        return $query->where('transaction_category', 'stock_in');
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return query of stock_in transaction_category
     *
     * @param $query
     */
    public function scopeStockOut($query)
    {
        return $query->where('transaction_category', 'stock_out');
    }

}
