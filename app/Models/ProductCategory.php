<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    

    /**
     * * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_category_name', 'product_category_description'];

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return relation from product model
     *
     */
    public function products() {
        return $this->hasMany(Product::class);
    }

}
