<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_name', 'product_description', 'product_image', 'product_category_id', 'stock'];


    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return relation from product_category model
     *
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Return relation from transaction log model
     *
     */
    public function logs() {
        return $this->hasMany(TransactionLog::class);
    }

}
