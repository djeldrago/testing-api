<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\TransactionLog;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Display a listing of the product resources.
     */
    public function index()
    {
        $products = Product::with('category')->get();
        return response()->json([
            'success' => 'true',
            'message' => 'Success load Produk',
            'data' => $products
        ], 200);
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Store a newly created product resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        // Validating request
        $this->validate($request, [
			'product_name' => 'required|max:30',
			'product_description' => 'required|max:100',
			'product_image' => 'required|max:2000',
			'product_category_id' => 'required',
			'stock' => 'required'
		]);
        $product = new Product;
        $product->product_name = $request->product_name;
        $product->product_description = $request->product_description;
        $product->product_category_id = $request->product_category_id;
        $product->stock = $request->stock;

        // Handling file naming
        if ($request->hasFile('product_image')) {
            $picture = $request->product_image;
            $productImageName = $picture->getClientOriginalName();
            $picture->move('uploads/product-image', $productImageName);
            $request->product_image = $productImageName;
        }
        $product->product_image = $request->product_image;
        $product->save();
        return response()->json([
            'success' => 'true',
            'message' => 'Success create Produk',
            'data' => $request->all()
        ], 201);
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Show specificaly product.
     *
     * @param $id
     */
    public function show($id)
    {
        $product = Product::find($id);
        if(empty($product)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Product',
            'data' => 'For id : ' . $id
        ], 404);
        } else {
            return response()->json([
            'success' => 'true',
            'message' => 'Success load Produk',
            'data' => [
                'product_name' => $product->product_name,
                'product_description' => $product->product_description,
                'product_image' => $product->product_image,
                'product_category' => $product->category->product_category_name,
                'stock' => $product->stock,
            ]
        ], 200);
        }
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Update an existing created product resource in storage.
     *
     * @param \Illuminate\Http\Request $request, $id
     */
    public function update(Request $request, $id)
    {
        // Validating request
        $this->validate($request, [
			'product_name' => 'required|max:30',
            'product_description' => 'required|max:100',
			'product_image' => 'required|max:2000',
			'product_category_id' => 'required',
			'stock' => 'required'
		]);
        $product = Product::where('id',$id)->first();
        if(empty($product)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Kategori Produk',
            'data' => 'For id : ' . $id
        ], 404);
        } else {
            $product->product_name = $request->product_name;
            $product->product_description = $request->product_description;
            $product->product_category_id = $request->product_category_id;
            $product->stock = $request->stock;
            if ($request->hasFile('product_image')) {
                // Handling file naming and delete existing image
                File::delete('uploads/product-image/'. $product->product_image);
                $picture = $request->product_image;
                $productImageName = $picture->getClientOriginalName();
                $picture->move('uploads/product-image', $productImageName);
                $banner->product_image = $productImageName;
                $request->product_image = $productImageName;
            }
            $product->product_image = $request->product_image;
            $product->save();
            return response()->json([
                'success' => 'true',
                'message' => 'Success update Produk',
                'data' => $request->all()
            ], 201);
        }   
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Delete an existing created product resource in storage.
     *
     * @param $id
     */
    public function destroy($id)
    {
        $product = Product::where('id',$id)->first();
        if(empty($product)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Produk',
            'data' => 'For id : ' . $id
        ], 404);
        } else {
            $product->delete();
            return response()->json([
                'success' => 'true',
                'message' => 'Success delete Produk'
            ], 201);
        }   
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Update a stock in an existing created product stock with input request.
     *
     * @param $request, $Id
     */
    public function stockIn(Request $request, $id) {
        $product = Product::where('id', $id)->first();
        if(empty($product)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Produk',
            'data' => 'For id : ' . $id
        ], 404);
        } else {
            $this->validate($request, [
                'transaction_amount' => 'required'
            ]);
            $product->stock = $product->stock + $request->transaction_amount;
            $product->save();

            $log = new TransactionLog;
            $log->product_id = $id;
            $log->transaction_category = 'stock_in';
            $log->transaction_amount = $request->transaction_amount;
            $log->save();
            return response()->json([
                'success' => 'true',
                'message' => 'Success stock in Produk',
                'data' => $request->transaction_amount
            ], 201);
        }
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Update a stock out an existing created product stock with input request.
     *
     * @param $request, $Id
     */
    public function stockOut(Request $request, $id) {
        $product = Product::where('id', $id)->first();
        $max_stock = $product->stock;
        $this->validate($request, [
			'transaction_amount' => 'required'
        ]);
        if(empty($product)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Produk',
            'data' => 'For id : ' . $id
        ], 404);
        } else {
            // Check if the input is less than the existing stock
            if($request->transaction_amount <= $max_stock) {
                $product->stock = $product->stock - $request->transaction_amount;
                $product->save();
        
                $log = new TransactionLog;
                $log->product_id = $id;
                $log->transaction_category = 'stock_out';
                $log->transaction_amount = $request->transaction_amount;
                $log->save();
    
                return response()->json([
                'success' => 'true',
                'message' => 'Success stock out Produk',
                'data' => $request->transaction_amount
            ], 201);
            } else {
                return response()->json([
                'success' => 'false',
                'message' => 'Current stock is less than stock out request'
            ], 400);
            }
        }
    }
}
