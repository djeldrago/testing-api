<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    
    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Display a listing of the product category resources.
     */
    public function index()
    {
        $categories = ProductCategory::select('product_category_name', 'product_category_description')->get();
        return response()->json([
            'success' => 'true',
            'message' => 'Success load Product Categories',
            'data' => $categories
        ], 200);
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Store a newly created category product resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        // Validating request
        $this->validate($request, [
			'product_category_name' => 'required|max:30',
			'product_category_description' => 'required|max:100'
		]);
        $category = new ProductCategory;
        $category->product_category_name = $request->product_category_name;
        $category->product_category_description = $request->product_category_description;
        $category->save();
        return response()->json([
            'success' => 'true',
            'message' => 'Success create Product Category',
            'data' => $request->all()
        ], 201);
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Show specificaly product category.
     *
     * @param $id
     */
    public function show($id)
    {
        $category = ProductCategory::where('id', $id)->select('product_category_name', 'product_category_description')->first();
        if(empty($category)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Product Category',
            'data' => 'For category id : ' . $id
            ], 400);
        } else {
            return response()->json([
            'success' => 'true',
            'message' => 'Success load Product Category',
            'data' => $category
            ], 200);
        }
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Update an existing created category product resource in storage.
     *
     * @param \Illuminate\Http\Request $request, $id
     */
    public function update(Request $request, $id)
    {
        // Validating request
        $this->validate($request, [
			'product_category_name' => 'required|max:30',
			'product_category_description' => 'required|max:100'
		]);
        $category = ProductCategory::where('id', $id)->first();
        if(empty($category)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Product Category',
            'data' => 'For category id : ' . $id
            ], 400);
        } else {
            $category->product_category_name = $request->product_category_name;
            $category->product_category_description = $request->product_category_description;
            $category->save();
            return response()->json([
                'success' => 'true',
                'message' => 'Success update Product Category',
                'data' => $request->all()
            ], 201);
        }   
    }
    
    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Delete an existing created category product resource in storage.
     *
     * @param $id
     */
    public function destroy($id)
    {
        $category = ProductCategory::where('id',$id)->first();
        if(empty($category)) {
            return response()->json([
            'success' => 'false',
            'message' => 'No Such Product Category',
            'data' => 'For category id : ' . $id
        ], 400);
        } else {
            $category->delete();
            return response()->json([
                'success' => 'true',
                'message' => 'Success delete Product Category'
            ], 201);
        }   
    }
}
