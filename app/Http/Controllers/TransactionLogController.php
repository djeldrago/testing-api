<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TransactionLog;
use App\Models\Product;
use Illuminate\Http\Request;

class TransactionLogController extends Controller
{

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * Display a listing of the transaction log resources.
     * @param $request
     */
    public function index(Request $request)
    {
        $transactionLogs = TransactionLog::with('products')->get();
        return response()->json([
            'success' => 'true',
            'message' => 'Success load Log Transaksi',
            'data' => $transactionLogs
        ], 200);
    }

    /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * return stock_in transaction_category
     * 
     */
    public function stockIn() {
        $transactionLogs = TransactionLog::stockIn()->get();
        return response()->json([
            'success' => 'true',
            'message' => 'Success load Stock In Log Transaksi',
            'data' => [
                ''
            ]
        ], 200);
    }

     /**
     * Author = Muhammad Ramdhan Syakirin (ramdhan.syakirin@omnitech.id)
     * return stock_out transaction_category
     * 
     */
    public function stockOut() {
        $transactionLogs = TransactionLog::stockOut()->get();
        return response()->json([
            'success' => 'true',
            'message' => 'Success load Stock Out Log Transaksi',
            'data' => $transactionLogs
        ], 200);
    }
}
