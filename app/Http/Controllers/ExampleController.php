<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function getKey()
    {
        $key = Str::random(32);
        return $key;
    }

    //
}
