<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api/v1'], function () use ($router) {
    // API For Kategori Produk
    $router->get('/categories', 'ProductCategoryController@index');
    $router->post('/categories', 'ProductCategoryController@store');
    $router->get('/categories/{id}', 'ProductCategoryController@show');
    $router->patch('/categories/{id}/update', 'ProductCategoryController@update');
    $router->delete('/categories/{id}/delete', 'ProductCategoryController@destroy');

    // API For Produk
    $router->get('/products', 'ProductController@index');
    $router->post('/products', 'ProductController@store');
    $router->get('/products/{id}', 'ProductController@show');
    $router->patch('/products/{id}/update', 'ProductController@update');
    $router->delete('/products/{id}/delete', 'ProductController@destroy');
    $router->patch('/products/{id}/stock-in', 'ProductController@stockIn');
    $router->patch('/products/{id}/stock-out', 'ProductController@stockOut');

    // API For Log Transaksi
    $router->get('/transaction-logs', 'TransactionLogController@index');
    $router->get('/transaction-logs/stock-in', 'TransactionLogController@stockIn');
    $router->get('/transaction-logs/stock-out', 'TransactionLogController@stockOut');
    $router->get('/transaction-logs/{id}', 'TransactionLogController@show');
});